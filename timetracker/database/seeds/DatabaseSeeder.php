<?php

use Illuminate\Database\Seeder;
use App\Client;

class DatabaseSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        //$this->call('UsersTableSeeder');

        function createClients(int $n, array $names, DateTimeZone $timezone) {
            
            $dateTime = new DateTime('now', $timezone);
            $now = $dateTime->format(DateTime::W3C);
            
            for($i = 0; $i < $n; $i++) {
                Client::create(['name' => $names[array_rand($names)]]);

                /* Old direct way see above for how to use the Eloquent\Model facade
                DB::table('clients')->insert([
                    'name' => $names[array_rand($names)],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);*/
            }
        }

        // get $companies
        $companies = require(__DIR__ . '/companies.php');
        createClients(10, $companies, new DateTimeZone(getenv('APP_TIMEZONE')));
    }
}
