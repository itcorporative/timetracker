<?php
 
namespace App\Http\Controllers;
 
use App\Client;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

function log($name, $request) {
    error_log($name);
    error_log($request);
    error_log("request->all()");
    error_log(print_r($request->all(), true));
    error_log("request->input('name')");
    error_log(print_r($request->input('name'), true));
    error_log("**********************************************");
}
 
class ClientController extends BaseController
{ 
    public function index(){
 
        $clients  = Client::all();
 
        return response()->json($clients);
 
    }
 
    public function getClient(int $id){
 
        $client  = Client::find($id);
 
        return response()->json($client);
    }
 
    public function saveClient(Request $request){
        log("saveClient", $request);

        $client = Client::create($request->all());
 
        return response()->json($client);
 
    }
 
    public function deleteClient(int $id){
        $client = Client::find($id);
 
        $client->delete();
 
        return response()->json('success');
    }
 
    public function updateClient(Request $request, int $id){
        log("updateClient", $request);
        
        $client = Client::find($id);
 
        $client->name = $request->input('name');
 
        $client->save();
 
        return response()->json($client);
    }

}