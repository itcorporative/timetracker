import babel from "rollup-plugin-babel"
import babelrc from "babelrc-rollup"
import uglify from 'rollup-plugin-uglify'
import { minify } from 'uglify-js'


// write which browsers we are building for (TODO: safer graph traversing)
const browsers = JSON.parse(require("fs").readFileSync("./.babelrc")).presets[0][1].targets.browsers.join(",")
console.log(`Compiling source to execute on ${require("browserslist")(browsers).join(", ")}`)


export default {
    entry: "js/src/boot/bootstrap.js",
    plugins: [
        babel(babelrc()),
        uglify({}, minify) // hack: https://github.com/TrySound/rollup-plugin-uglify#warning
    ],
    /*context: "window",
    globals: {
        app: 'window',
    },*/
    targets: [{
        dest: "js/build/bootstrap.js",
        format: "iife",
        moduleName: "app",
        sourceMap: true
    }]
}

