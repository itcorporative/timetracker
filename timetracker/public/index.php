<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| First we need to get an application instance. This creates an instance
| of the application / container and bootstraps the application so it
| is ready to receive HTTP / Console requests from the environment.
|
*/

$app = require __DIR__.'/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/

# $app->run() doesn't work if the website root is not the root of timetracker e.g. MAMP
# see https://stackoverflow.com/questions/30891405/not-found-page-in-lumen-after-install
# and https://stackoverflow.com/questions/29728973/notfoundhttpexception-with-lumen
$app->run();
#$app->run($app['request']);
