"use strict"

class Client {
    constructor(properties) {
        this.id = properties.id
        this.name = properties.name
        this.created_at = properties.created_at
        this.updated_at = properties.updated_at
    }

    html() {
        console.log("html method called")
    }
}

export {
    Client
}