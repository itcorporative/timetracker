"use strict"

import { identity} from "app"
import axios from "axios"

//const identity = app.identity

/**
 * Pure mixin - the passed in objects are not touched
 * @param {Object} proto This will be the prototype of the returned object
 * @param {Object} object These properties will be properties of the returned object
 * @returns {Object} A new object with the properties of the two passed in objects
 */
function mixin(proto, object) {
    return Object.create(proto, propertyBuilder(object))
}


function propertyBuilder(object) {
    const propertyNames = Object.keys(object)
    const properties = {}

    propertyNames.forEach(property => {
        properties[property] = { value: object[property] }
    })
    return properties
}


function each(f, collection) {
    if(collection.length) eachList(f, collection)
    else eachObject(f, collection)
}


function eachList(f, list) {
    for(let i = 0, len = list.length; i < len; i++) {
        f(list[i])
    }
}


function eachObject(f, obj) {
    for(let prop in obj) {
        if(obj.hasOwnProperty(prop))
            f(obj[prop], prop)
    }
}


function map(f, collection) {
    return collection.length ? mapList(f, collection) : mapObject(f, collection)
}


function mapList(f, list) {
    const mapped = []
    for(let i = 0, len = list.length; i < len; i++) {
        mapped.push(f(list[i]))
        /*const result = f(list[i])
        if(result != null) mapped.push(result)*/
    }
    return mapped
}


function mapObject(f, obj) {
    const mapped = {}
    for(let prop in obj) {
        if(!obj.hasOwnProperty(prop)) continue
        mapped[prop] = f(obj[prop], prop)
        /*const result = f(obj[prop], prop)
        if(result != null) mapped[prop] = result*/
    }
    return mapped
}

/*function filter(f, collection) {
    const filtered
    map(item => , collection)
    return filtered
}*/

/**
 * ajax
 * @param {String} url
 * @param {String} method
 * @param {HTMLFormElement} [form]
 * @param {String} [type]
*/
function ajax(url, method, form, type) {
    const http = new XMLHttpRequest()
    const asyncAction = (success, failure) => {
        http.onreadystatechange = () => {

            //console.log(http.responseType)

            if(http.readyState === XMLHttpRequest.DONE && http.status === 200) {
                success( http.response )
            }
        }
    }
    const promise = new Promise(asyncAction)

    http.responseType = type ? type : "json"

    console.log("ajax", method, url)
    http.open(method, url)

    // prepare the form data to be send
    switch(method.toLowerCase()) {
        case "get":
            http.send()
            break
        case "put":
            // use Illuminate\Http\Request does NOT support multipart/form-data with PUT method
            const data = map(el =>
                el.name && [el.name, el.value], form.elements) // make tuples of key/value
                .filter(identity) // remove falsy indices
                .reduce((dataString, pair) =>
                    dataString += encodeURI(pair.join("=")) + "&", "") // make string as "k=v&k=v&"
                .replace(/&$/, "") // remove last &

            console.log(data)

            http.setRequestHeader("Content-Type", form.enctype || "application/x-www-form-urlencoded")
            http.send(data)
            break
        case "delete":
        case "post":
            // FormData will set content-type to multipart/form-data; boundary
            http.send(new FormData(form))
            break
    }

    return { promise, http }
}

export {
    ajax,
    each,
    map,
    propertyBuilder,
    mixin
}
