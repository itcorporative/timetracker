let queue = []

document.addEventListener("readystatechange", executeQueue)

function executeQueue() {
    switch(document.readyState) {
        case "loading":
            break
        case "interactive": // firefox has loaded our scripts
        case "complete": // chrome has loaded our scripts
            const currentSet = queue.map(f =>
                f[1]()
                && f[0]
                || null
            ).filter(identity)

            if(currentSet.length) currentSet.forEach(f => { f() })
            queue = queue.filter(f => !~currentSet.indexOf( f[0] ))
            if(queue.length === 0) document.removeEventListener("readystatechange", executeQueue)

            console.debug(queue, "should be empty")
            break
        default:
            console.warn("What is this madness?")
    }
    if(document.readyState === "complete" && queue.length)
        console.warn("Unrun functions in queue", queue)
}

function onRender(f, predicate = truthy) {
   queue.push([f, predicate])
}

function truthy() { return true }
function identity(x) { return x }


export {
    onRender,
    identity   
}