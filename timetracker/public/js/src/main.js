import { ajax, each, map } from "./utilities.js"


window.app.clientFillForm = function(form) {
    console.log(`clientFillForm called from ${form.getAttribute("id")}`)

    /*ajax(
        interpolateFormAction(form),
        form.getAttribute("method"),
        form
    )*/
    // request clients -> request client id -> fill form
    ajax("api/clients", "get")
        .promise
        .then(response => {
            const id = response[0] && response[0].id
            if(!id) return
            ajax(`api/client/${id}`, "get")
                .promise
                .then(response => {
                    each(field => {
                        if(response[field.name])
                            field.value = response[field.name]
                    }, form)
                })
        })
}

each(ajaxify, document.forms)

/**
 * Will interpolate brackets {name} in the form action attribute,
 * into the value of an input field with the same name.
 * @param {HTMLFormElement} form
 * @returns {String}
 */
function interpolateFormAction(form) {
    // interpolate url if {} is used
    const url = decodeURIComponent(form.action)
    const tokenMatcher = /({([^\s]+)})/
    return url.replace(tokenMatcher, token => {
        console.log(token)
        return form.elements[
            token.match(tokenMatcher)[2]
        ].value
    })
}

function interpolateString(url, dictionary) {

}

function ajaxify(form, handler) {
    /**
        NOTE: form.method can not be used for PUT and DELETE
        PUT and DELETE is not supported, so `form.method == "get"` when they are used.
        Instead we can read the attribute directly via `form.getAttribute("method")`
        See http://softwareengineering.stackexchange.com/questions/114156/why-are-there-are-no-put-and-delete-methods-on-html-forms#answer-211790
    **/
    form.addEventListener("submit", event => {
        event.preventDefault()

        const url = interpolateFormAction(form)

        clientListResponse(
            ajax(url, form.getAttribute("method"), event.target),
            event.target
        )
    })

    console.log(`ajaxify ${form.getAttribute("id")}`)
}

function clientListResponse(reponse, target) {
    reponse
        .promise
        .then(json => {
            const htmlList = document.createElement("ol")
            const lis = []
            each( client => {
                const li = document.createElement("li")
                li.innerText = client.name
                lis.push(li)
            }, Array.isArray(json) ? json : [json])
            each(li => {
                htmlList.appendChild(li)
            }, lis)

            target
                .querySelector(".client-list")
                .appendChild(htmlList)
        })
}

function clientFormResponse(reponse, target) {
    reponse
        .promise
        .then(json => {

        })
}