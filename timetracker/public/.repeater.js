#!/usr/bin/env node

const command = process.argv.slice(2).join(" ")
const exec = require("child_process").exec

/*console.log(command)
console.log(process.cwd())
console.log(process.argv)
*/
const time = 2000
let lastLog = "",
    lastWarning = "",
    running = false

setInterval( () => {
    if(running) return
    running = true
    exec(command, {
        cwd: process.cwd()
    }, report, time )
}, time)

function report(error, stdout, stderr) {
    if(error) {
        console.error(`repeater error: ${error}`)
        process.exit(1)
    }

    if(lastLog !== stdout) {
        console.log(stdout)
        lastLog = stdout
    }

    if(lastWarning !== stderr) {
        console.warn(stderr)
        lastWarning = stderr
    }

    running = false
}