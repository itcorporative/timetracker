import babel from "rollup-plugin-babel"
import babelrc from "babelrc-rollup"
import camelCase from "camelcase"
import uglify from 'rollup-plugin-uglify'
import { minify } from 'uglify-js'


const pkg = require("./package.json")
//const external = getExternalDependencies(pkg)

// write which browsers we are building for (TODO: safer graph traversing)
const browsers = JSON.parse(require("fs").readFileSync("./.babelrc")).presets[0][1].targets.browsers.join(",")
console.log(`Compiling source to execute on ${require("browserslist")(browsers).join(", ")}`)


export default {
    entry: "js/src/main.js",
    plugins: [
        babel(babelrc()),
        uglify({}, minify) // hack: https://github.com/TrySound/rollup-plugin-uglify#warning
    ],
    globals: {
        app: "window.app"
    },
  //  external,
    targets: [{
        dest: pkg["main"],
        format: "iife",
        moduleName: camelCase(pkg["name"]),
        sourceMap: true
    }]
}


function getExternalDependencies(pkg) {
    const dependencies = Object.keys(pkg.dependencies || {})
    return dependencies.concat(Object.keys(pkg.devDependencies || {}))
}
