<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Http\Response;

function log_request_path($path) {
    error_log($path);
}

$app->get('/version', function() use ($app) {
    log_request_path("/version");
    return $app->version();
});

$app->get('/', function() {
    log_request_path("/");
    return redirect()->route('page', ['page' => 'index']);
});

$app->get('/favicon.ico', function() {
    log_request_path("/favicon.ico");
    return Response::create('<h1>favicon.ico not found</h1>', 404);
});

$app->get('/{page}', ['as' => 'page', function($page) use ($app) {
    log_request_path("/" . $page);

    $base = $app->basePath();
    $lang = getenv('APP_LOCALE');// ?? getenv('APP_FALLBACK_LOCALE') ?? 'en';
    
    #echo "{$base}/public/html/{$page}.phtml";
    # Response doesn't execute php in phtml 
    #$content = $app->files->get("{$base}/public/phtml/{$page}.phtml");
    #error_log($content);
    #return new Response($content);
	require("{$base}/public/phtml/{$page}.phtml");
}]);

$app->get('/js/build/{path}', function($path) use ($app) {
    log_request_path("/js/build/" . $path);
    $base = $app->basePath();

    $content = $app->files->get("{$base}/public/js/build/{$path}");

    #print_r($app);
    
    return (new Response($content))
        ->header('Content-Type', 'application/javascript');
});

$app->get('/js/vendor/{path}', function($path) use ($app) {
    log_request_path("/js/vendor/" . $path);
    $base = $app->basePath();

    $content = $app->files->get("{$base}/public/js/vendor/{$path}");
    
    return (new Response($content))
        ->withHeaders([
            'Content-Type', 'application/javascript',
            'Cache-Control', 'public'
        ]);
});

/** REST API **/

$app->get('api/clients', 'ClientController@index');
 
$app->get('api/client/{id}','ClientController@getClient');
 
$app->post('api/client','ClientController@saveClient');
 
$app->put('api/client/{id}','ClientController@updateClient');
 
$app->delete('api/client/{id}','ClientController@deleteClient');
