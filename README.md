TimeTracker
===========

Simpel time tracker using the [Laravel Lumen framework](https://lumen.laravel.com/).

## Prerequisites ##

Lumen requires PHP 5.6.4 but I have only tested on PHP 7.0.8+

Lumen also requires [Composer](https://getcomposer.org/) which can be install via homebrew on Mac,
just type `brew install homebrew/php/composer` in your terminal.

## Installation ##

`cd` into the timetracker directory if you are not already there.
Then install the dependencies via Composer: `composer install`

You can check that everything is working by calling the _artisan_ cli helper. `./artisan list`

Next you need to set the environment to match your database setup, by filling out the `.env` file.

Copy `.env.example` to `.env` and open `.env`.

It should look like this:

```
APP_ENV=local
APP_DEBUG=true
APP_KEY=
APP_TIMEZONE=UTC
APP_LOCALE=da

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret

CACHE_DRIVER=memcached
QUEUE_DRIVER=sync
```

Change it to match your configuration.
In my case using MAMP, it's:

```
APP_ENV=local
APP_DEBUG=true
APP_KEY=secret7EncryptoKey!
APP_TIMEZONE=UTC
APP_LOCALE=da

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=8889
DB_DATABASE=timetracker_db
DB_USERNAME=root
DB_PASSWORD=root

CACHE_DRIVER=memcached
QUEUE_DRIVER=sync
```

Next we have to create the database `timetracker_db` or what ever database name you put in your `.env` configuration.

If you use MAMP, then open your browser and go to
[http://localhost:8888/MAMP/index.php?page=phpmyadmin&language=English](http://localhost:8888/MAMP/index.php?page=phpmyadmin&language=Englis).
You should know how to create a dabase in phpMyAdmin...

If ran the `./artisan list` command previously you saw all the options available to you. Some of them are for creating test data (and deleting them again).
Just to get a feel for the app, lets create some test data and delete them before you start entering your own data.

1. `./artisan migrate` - create tables in the db
2. `./artisan db:seed` - create test data
3. `./artisan migrate:reset` - delete all data in the db
4. `./artisan migrate:rollback` - delete the data inserted via the last seed (2).


## Start/Stop application ##

**Start**: In the console, type `./timetracker/artisan serve`.

**Stop**: In the console, press `Ctrl`+`C`.


## Create New Test Data ##

If you want to delete the test data and add a new set then you can do this quickly.
In the console, type `./timetracker/artisan migrate:refresh --seed`
